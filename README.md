# 20181213 - Deref.rs \#1

Thu, 13. Dec. 2018

https://derefrs.connpass.com/event/111957/

```toml
[meetup]
name = "Deref Rust"
date = "20181213"
version = "0.0.0"
attendees = [
  "Yasuhiro Asaka <yasuhiro.asaka@grauwoelfchen.net>"
]
repository = "https://gitlab.com/derefrs/reading-logs/20181213-1"
keywords = ["Rust"]

[locations]
park6 = {site = "Roppongi"}
zulip = {site = "https://derefrs.zulipchat.com", optional = true }
```

## Notes

| Name | Snippet / Note / What I did in few words |
|--|--|
| @grauwoelfchen | [Read Chapter 13. of "The Rust Programming Language" (Second Edition)](https://nostarch.com/Rust) |


## Links

* [Deref Rust - GitLab.com](https://gitlab.com/derefrs)
* [Deref Rust - Zulip Chat](https://derefrs.zulipchat.com/)

## License

`MIT`

```text
Reading Logs
Copyright (c) 2018 Deref.rs

This is free software: You can redistribute it and/or modify
it under the terms of the MIT License.
```
